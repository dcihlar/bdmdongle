#ifndef _CPPPIN_H_
#define _CPPPIN_H_

#include <Arduino.h>
#include <stdint.h>


#define CPPPIN(name, ofs)	CPPPin{&DDR##name, &PORT##name, &PIN##name, ofs}

class CPPPin {
	public:
		constexpr CPPPin(volatile uint8_t *dirport, volatile uint8_t *outport, volatile uint8_t *inport, uint8_t bit=0)
			: dirport(dirport), outport(outport), inport(inport), mask(1 << bit)
		{}

		void set_in_dir() const  { *dirport &= ~mask; }
		void set_out_dir() const { *dirport |=  mask; }

		void write(bool state) const {
			if (state) {
				*outport |=  mask;
			} else {
				*outport &= ~mask;
			}
		}

		bool read() const {
			return !!(*inport & mask);
		}

		void toggle() const {
			*inport |= mask;
		}

	private:
		volatile uint8_t *dirport;
		volatile uint8_t *outport;
		volatile uint8_t *inport;
		const uint8_t mask;
};

static inline void digitalWrite(const CPPPin &pin, uint8_t state) {
	pin.write(state != LOW);
}

static inline bool digitalRead(const CPPPin &pin) {
	return pin.read();
}

static inline void pinMode(const CPPPin &pin, uint8_t mode) {
	switch (mode) {
		case INPUT:
			pin.set_in_dir();
			break;
		case INPUT_PULLUP:
			pin.set_in_dir();
			pin.write(true);
			break;
		case OUTPUT:
			pin.set_out_dir();
			break;
	}
}

#endif
