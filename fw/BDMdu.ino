#include <avr/pgmspace.h>
#include <stdarg.h>
#include <SPI.h>
#include "cpppin.h"


#define BDM_CMD_RDREG	0x2180U
#define BDM_CMD_WRREG	0x2080U
#define BDM_CMD_RSREG	0x2580U
#define BDM_CMD_WSREG	0x2480U
#define BDM_CMD_READ	0x1900U
#define BDM_CMD_WRITE	0x1800U
#define BDM_CMD_DUMP	0x1D00U
#define BDM_CMD_FILL	0x1C00U
#define BDM_CMD_GO	 	0x0C00U
#define BDM_CMD_CALL	0x0800U
#define BDM_CMD_RST		0x0400U
#define BDM_CMD_NOP		0x0000U
#define BDM_SIZE_CMD		17
#define BDM_SIZE_BYTE		0x00
#define BDM_SIZE_WORD		0x40
#define BDM_SIZE_LONG		0x80
#define BDM_RSP_NOTREADY	0x10000UL
#define BDM_RSP_BERR        0x10001UL
#define BDM_RSP_ILLEGAL     0x1FFFFUL
#define BDM_RSP_CMDCMPLTE   0x0FFFFUL
#define BDM_REG_RPC         0x0
#define BDM_REG_PCC         0x1
#define BDM_REG_SR          0xb
#define BDM_REG_USP         0xc
#define BDM_REG_SSP         0xd
#define BDM_REG_SFC         0xe
#define BDM_REG_DFC         0xf
#define BDM_REG_ATEMP       0x8
#define BDM_REG_FAR         0x9
#define BDM_REG_VBR         0xa


static const byte pin_step = 8;
static const byte pin_berr = 6;
static const byte pin_freeze = 9;
static const byte pin_rst = 7;
static const byte pin_vsw = A4;
static const byte pin_vbus = A5;
static constexpr auto pin_req_vsw = CPPPIN(D, 4);
static const byte pin_vsw_fault = 0;
static const byte pin_led = 13;
static const byte pin_flash_cs = 11;
static constexpr auto pin_dsclk = CPPPIN(C, 6);
static constexpr auto pin_dsi = CPPPIN(B, 6);
static constexpr auto pin_dso = CPPPIN(D, 6);

static bool mode_bin = false;
static byte have_vdd = 0xff;


#define CM_ANY		0x0
#define CM_OPERABLE	0x1	// target has power and is not reset
#define CM_FROZEN	0x2
#define CM_DBG		(CM_OPERABLE | CM_FROZEN)
struct BDMCommand {
	char name;
	uint8_t allowed_mode;
	void (*handler)();
	const char *format;
	const char *args;
	const char *desc;

	constexpr bool is_valid() const { return name != 0; }
};

struct BDMRegister {
	char name[4];
	bool is_sys : 1;
	uint8_t id : 7;

	constexpr bool is_valid() const { return *name != 0; }
	constexpr bool in_set(char set) const {
		return (set != 'g') ||
		       (!is_sys || (id == BDM_REG_RPC || id == BDM_REG_SR));
	}

	bool read(uint32_t &val) const;
	bool write(uint32_t val);

	static BDMRegister find(const char *name);
};


template<typename T>
class IterFlash {
	private:
		const T *const mem_start;

		class Iter {
			private:
				const T *mem_ptr;
				T data;

				bool is_valid() const;
				bool is_end() const { return !mem_ptr || !is_valid(); }

				void load() {
					memcpy_P(&data, mem_ptr, sizeof(T));
				}

			public:
				Iter(const T* data = nullptr)
					: mem_ptr(data)
				{
					load();
				}

				bool operator != (const Iter &other) const {
					return mem_ptr != other.mem_ptr && is_end() != other.is_end();
				}

				Iter& operator++() {
					++mem_ptr;
					load();
					return *this;
				}

				T operator*() const {
					return data;
				}
		};

	public:
		constexpr IterFlash(const T* data)
			: mem_start(data)
		{}

		Iter begin() const {
			return {mem_start};
		}

		Iter end() const {
			return {};
		}
};

template<>
bool IterFlash<BDMCommand>::Iter::is_valid() const { return data.is_valid(); }
template<>
bool IterFlash<BDMRegister>::Iter::is_valid() const { return data.is_valid(); }
template<>
bool IterFlash<char>::Iter::is_valid() const { return data != '\0'; }


static volatile bool was_vsw_fault = false;
static void on_vsw_fault() {
	digitalWrite(pin_req_vsw, LOW);
	was_vsw_fault = true;
}

void setup() {
	digitalWrite(pin_req_vsw, LOW);
	pinMode(pin_req_vsw, OUTPUT);
	pinMode(pin_berr, INPUT);
	pinMode(pin_freeze, INPUT);
	pinMode(pin_rst, INPUT);
	pinMode(pin_dsi, OUTPUT);
	pinMode(pin_dso, INPUT);
	pinMode(pin_dsclk, OUTPUT);
	digitalWrite(pin_step, LOW);
	pinMode(pin_step, OUTPUT);

	/*
	 * note: It takes time for this to stabilize.
	 *       First few reads may be inaccurate.
	 */
	analogReference(INTERNAL);

	pinMode(pin_vsw_fault, INPUT);
	attachInterrupt(digitalPinToInterrupt(pin_vsw_fault), on_vsw_fault, FALLING);

	pinMode(pin_led, OUTPUT);
	digitalWrite(pin_led, HIGH);

	Serial.begin(115200);
	digitalWrite(pin_flash_cs, HIGH);
	pinMode(pin_flash_cs, OUTPUT);

	SPI.begin();
}

static bool target_has_vdd() {
	return have_vdd == HIGH;
}

static bool target_is_reset() {
	return target_has_vdd() && !digitalRead(pin_rst);
}

static bool target_is_frozen() {
	return digitalRead(pin_freeze);
}

uint32_t send_cmd(uint16_t cmd, uint8_t count = 17) {
	union {
		__uint24 data;
		uint8_t bytes[3];
	} u;
	u.data = (__uint24)cmd << (24 - count);

	/* Note:
	 *   Do this ASAP!
	 *   Final clock pulse must be shorter than executing one instruction!
	 */
	cli();
	while (count--) {
		bool wbit = !!(u.bytes[2] & 0x80);
		u.data <<= 1;
		u.bytes[0] |= digitalRead(pin_dso);
		digitalWrite(pin_dsi, wbit);
		digitalWrite(pin_dsclk, HIGH);
		digitalWrite(pin_dsclk, LOW);
	}
	sei();
	return u.data;
}

static void dump_byte_ascii(uint8_t data) {
	static const char hex[] PROGMEM = "0123456789ABCDEF";
	Serial.print((char)pgm_read_byte(&hex[data >> 4]));
	Serial.print((char)pgm_read_byte(&hex[data & 15]));
}

static void dump_ret(uint32_t data, uint8_t szflags = BDM_SIZE_LONG) {
	if (mode_bin) {
		union {
			uint32_t data32;
			uint16_t data16;
			uint8_t data8;
		} bdata;
		const uint8_t *wdata;
		size_t wsize;

		if (szflags & BDM_SIZE_LONG) {
			bdata.data32 = data;
			wdata = reinterpret_cast<uint8_t*>(&bdata.data32);
			wsize = sizeof(uint32_t);
		} else if (szflags & BDM_SIZE_WORD) {
			bdata.data16 = data;
			wdata = reinterpret_cast<uint8_t*>(&bdata.data16);
			wsize = sizeof(uint16_t);
		} else {
			bdata.data8 = data;
			wdata = &bdata.data8;
			wsize = sizeof(uint8_t);
		}
		Serial.write(wdata, wsize);
	} else {
		if (szflags & BDM_SIZE_LONG) {
			dump_byte_ascii((uint8_t)(data >> 24));
			dump_byte_ascii((uint8_t)(data >> 16));
		}
		if (szflags & (BDM_SIZE_LONG | BDM_SIZE_WORD)) {
			dump_byte_ascii((uint8_t)(data >>  8));
		}
		dump_byte_ascii((uint8_t)(data >>  0));
		Serial.println();
	}
}

static void dump_one_ret(uint32_t data, uint8_t szflags = BDM_SIZE_LONG) {
	if (mode_bin) {
		Serial.println(F("BIN"));
	}
	dump_ret(data, szflags);
}

static bool bdm_clrerror(void)
{
	uint8_t ttl = 255;
	send_cmd(BDM_CMD_NOP);
	send_cmd(BDM_CMD_NOP);
	send_cmd(BDM_CMD_NOP);
	send_cmd(BDM_CMD_NOP);
	while (ttl &&  send_cmd(0, 1)) --ttl;
	while (ttl && !send_cmd(0, 1)) --ttl;
	if (!ttl) return false;
	send_cmd(0, 15);
	return true;
}

static bool bdm_request(uint16_t cmd, uint32_t &result, uint8_t nargs = 0, ...) {
	uint32_t response;
	va_list arg;
	bool is_ok = true;

	result = 0;

	// send command and arguments
	send_cmd(cmd);
	va_start(arg, nargs);
	while (nargs--) {
		if ((response = send_cmd(va_arg(arg, uint16_t))) > BDM_RSP_NOTREADY) {
			if (response == BDM_RSP_BERR) {
				is_ok = false;
				continue;
			}
		}
	}
	va_end(arg);

	// read response
	for (byte count = (cmd & BDM_SIZE_LONG) ? 2 : 1; count; count--) {
		while ((response = send_cmd(BDM_CMD_NOP)) == BDM_RSP_NOTREADY);
		if (response < BDM_RSP_NOTREADY) {
			result <<= 16;
			result |= response;
		} else if (response == BDM_RSP_BERR) {
			is_ok = false;
			break;
		}
	}
	if (response > BDM_RSP_NOTREADY) {
		if (response == BDM_RSP_BERR) {
			is_ok = bdm_clrerror();
		}
	}

	return is_ok;
}

static bool bdm_request_w(uint16_t cmd, uint32_t &result, uint16_t arg0) {
	return bdm_request(cmd, result, 1, arg0);
}

static bool bdm_request_w(uint16_t cmd, uint16_t arg0) {
	uint32_t dummy;
	return bdm_request_w(cmd, dummy, arg0);
}

static bool bdm_request_l(uint16_t cmd, uint32_t &result, uint32_t arg0) {
	return bdm_request(cmd, result, 2, (uint16_t)(arg0 >> 16), (uint16_t)(arg0 & 0xffffu));
}

static bool bdm_request_l(uint16_t cmd, uint32_t arg0) {
	uint32_t dummy;
	return bdm_request_l(cmd, dummy, arg0);
}

static bool bdm_request_lw(uint16_t cmd, uint32_t &result, uint32_t arg0, uint16_t arg1) {
	return bdm_request(cmd, result, 3, (uint16_t)(arg0 >> 16), (uint16_t)(arg0 & 0xffffu), arg1);
}

static bool bdm_request_lw(uint16_t cmd, uint32_t arg0, uint16_t arg1) {
	uint32_t dummy;
	return bdm_request_lw(cmd, dummy, arg0, arg1);
}

static bool bdm_request_ll(uint16_t cmd, uint32_t &result, uint32_t arg0, uint32_t arg1) {
	return bdm_request(cmd, result, 4, (uint16_t)(arg0 >> 16), (uint16_t)(arg0 & 0xffffu),
	                                   (uint16_t)(arg1 >> 16), (uint16_t)(arg1 & 0xffffu));
}

static bool bdm_request_ll(uint16_t cmd, uint32_t arg0, uint32_t arg1) {
	uint32_t dummy;
	return bdm_request_ll(cmd, dummy, arg0, arg1);
}


class SerialProto {
	public:
		void wait_ch() {
			while (Serial.available() < 1);
		}

		void eat_spaces() {
			for (;;) {
				wait_ch();
				if (Serial.peek() != ' ')
					break;
				Serial.read();
			}
		}

		uint16_t read_sz_flags() {
			eat_spaces();
			char sz = Serial.peek();
			switch (sz) {
				case 'w': Serial.read(); return BDM_SIZE_WORD;
				case 'l': Serial.read(); return BDM_SIZE_LONG;
				case 'b': Serial.read(); return BDM_SIZE_BYTE;
				default: return BDM_SIZE_BYTE;
			}
		}

		uint32_t read_u32() {
			uint32_t val = 0;
			char ch;

			eat_spaces();
			for (uint8_t i = 0; i < 32/4; ++i) {
				wait_ch();
				ch = Serial.peek();
				if (ch >= '0' && ch <= '9') {
					val <<= 4;
					val |= ch - '0';
				} else if ((ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F')) {
					val <<= 4;
					val |= (ch & 0xf) + 9;
				} else {
					break;
				}
				Serial.read();
			}
			return val;
		}

		void read_str(char *buf, uint8_t max_len) {
			char ch;
			eat_spaces();
			for (uint8_t i = 0; i < max_len - 1; ++i) {
				wait_ch();
				ch = Serial.peek();
				if (is_whitespace(ch))
					return;
				*(buf++) = ch;
				*buf = 0;
				Serial.read();
			}
		}

		char read_ch() {
			eat_spaces();
			char ch = Serial.peek();
			if (!is_newline(ch)) Serial.read();
			return ch;
		}

		uint32_t read_bin(uint16_t szflags = BDM_SIZE_LONG) {
			if (szflags & BDM_SIZE_LONG) {
				uint32_t ret;
				Serial.readBytes((char*)&ret, sizeof(ret));
				return ret;
			} else if (szflags & BDM_SIZE_WORD) {
				uint16_t ret;
				Serial.readBytes((char*)&ret, sizeof(ret));
				return ret;
			} else {
				uint8_t ret;
				Serial.readBytes((char*)&ret, sizeof(ret));
				return ret;
			}
			return 0;
		}

		uint32_t read_abu32(uint16_t szflags = BDM_SIZE_LONG) {
			return mode_bin ? read_bin(szflags) : read_u32();
		}

		bool read_abort() {
			return !Serial || Serial.available() > 0;
		}

	private:
		static bool is_newline(int c) {
			return c == '\n' || c == '\r';
		}

		static bool is_whitespace(int c) {
			return c == ' ' || is_newline(c);
		}
} proto;


class ArgStorage {
	public:
		template<typename T>
		T get() {
			if (!have_space_for(sizeof(T)))
				return T{};
			return get_next<T>();
		}

	protected:
		void rewind() {
			top = data;
		}

		template<typename T>
		bool add(const T& data) {
			if (!have_space_for(sizeof(T)))
				return false;
			get_next<T>() = data;
			return true;
		}

	private:
		uint8_t data[16] = {};
		uint8_t *top = data;

		constexpr bool have_space_for(uint8_t n_bytes) const {
			return (data + n_bytes) <= (data + sizeof(data));
		}

		template<typename T>
		T& get_next() {
			T *p = (T*)top;
			top += sizeof(T);
			return *p;
		}

};

class ArgParser : public ArgStorage {
	public:
		bool parse(const char *fmt) {
			rewind();

			if (!fmt) {
				return flush();
			}

			for (char c : IterFlash<char>(fmt)) {
				switch (c) {
					case 'w':
						if (!add(proto.read_sz_flags()))
							return false;
						break;
					case 'u':
						if (!add(proto.read_u32()))
							return false;
						break;
					case 'c':
						if (!add(proto.read_ch()))
							return false;
						break;
					case 'R':
						if (!add(get_register()))
							return false;
						break;
					default:
						flush();
						return false;
				}
			}
			rewind();

			return flush();
		}

		bool flush() {
			bool had_garbage = false;
			int ch;
			do {
				ch = Serial.read();
				had_garbage |= ch > 0 && ch != '\n' && ch != '\r';
			} while (ch != '\n' && ch != '\r');
			return !had_garbage;
		}

	private:
		BDMRegister get_register() {
			char name[4];
			proto.read_str(name, sizeof(name));
			return BDMRegister::find(name);
		}
} cmd_args;

static uint8_t orig_sfc, orig_dfc;
static bool supervisor_enabled = false;

static void supervisor_reset() {
	supervisor_enabled = false;
}

static void mmap_load();

static void supervisor_enable() {
	uint32_t tmp;

	if (supervisor_enabled)
		return;

	// save original access mode
	bdm_request(BDM_CMD_RSREG | BDM_REG_SFC, tmp); orig_sfc = tmp;
	bdm_request(BDM_CMD_RSREG | BDM_REG_DFC, tmp); orig_dfc = tmp;

	// memory access as "supervisor data"
	bdm_request_l(BDM_CMD_WSREG | BDM_REG_SFC, 5);
	bdm_request_l(BDM_CMD_WSREG | BDM_REG_DFC, 5);

	supervisor_enabled = true;

	// load memory regions
	mmap_load();
}

static void supervisor_leave() {
	// restore access mode
	if (!supervisor_enabled)
		return;
	bdm_request_l(BDM_CMD_WSREG | BDM_REG_SFC, orig_sfc);
	bdm_request_l(BDM_CMD_WSREG | BDM_REG_DFC, orig_dfc);
	supervisor_enabled = false;
}

struct MemRegion {
	uint32_t addr_start;
	uint32_t addr_end;
	bool readable : 1;
	bool writeable : 1;
	bool valid : 1;
};

static MemRegion mmap[13];

static bool mmap_check_addr(uint32_t addr) {
	supervisor_enable();
	// is it system register?
	if (addr >= 0xFFF000ul && addr <= 0x1000000ul)
		return true;
	// is it external memory?
	for (uint8_t csid = 0; csid < 13; ++csid) {
		if (addr >= mmap[csid].addr_start && addr < mmap[csid].addr_end)
			return true;
	}
	return false;
}

static bool mem_read(uint32_t &ret, uint32_t addr, uint8_t szflags = BDM_SIZE_LONG) {
	return mmap_check_addr(addr) && bdm_request_l(BDM_CMD_READ | szflags, ret, addr);
}

static bool mem_write(uint32_t data, uint32_t addr, uint8_t szflags = BDM_SIZE_LONG) {
	if (!mmap_check_addr(addr))
		return false;

	bool is_ok;
	if (szflags == BDM_SIZE_LONG) {
		is_ok = bdm_request_ll(BDM_CMD_WRITE | szflags, addr, data);
	} else {
		is_ok = bdm_request_lw(BDM_CMD_WRITE | szflags, addr, data);
	}

	// reload mmap if updated through this function
	if (is_ok && (
	    (addr >= 0xFFFA48ul && addr < 0xFFFA78ul) ||
	    (addr == 0xFFFB04ul) ||
	    (addr == 0xFFFE00ul))) {
		mmap_load();
	}
	return is_ok;
}

static uint16_t sim_reg_read(uint16_t addr) {
	uint32_t data;
	mem_read(data, 0xFFF000ul | addr, BDM_SIZE_WORD);
	return data;
}

static bool mmap_is_tpuram_available() {
	const uint16_t tpumcr = sim_reg_read(0xE00u);
	return !(tpumcr & (1u << 10));
}

void mmap_load() {
	static const uint32_t blksizes[8] PROGMEM = {
		0x800,
		0x2000,
		0x4000,
		0x10000,
		0x20000,
		0x40000,
		0x80000,
		0x100000
	};

	// from chip selects
	for (uint8_t csid = 0; csid < 12; ++csid) {
		const uint32_t csbar = sim_reg_read(0xA48u + csid * 4);
		const uint32_t csopt = sim_reg_read(0xA4Au + csid * 4);
		mmap[csid].addr_start = (csbar & 0xFFFFFFF8ul) << 8;
		mmap[csid].addr_end = mmap[csid].addr_start + pgm_read_dword(blksizes + (csbar & 7));
		mmap[csid].readable = !!(csopt & (1u << 11));
		mmap[csid].writeable = !!(csopt & (1u << 12));
		mmap[csid].valid = (csopt & (3u << 13)) != 0;
	}

	// from TPU RAM
	uint16_t trambar;
	if (!mmap_is_tpuram_available() || ((trambar = sim_reg_read(0xB04u)) & 1)) {
		mmap[12].valid = false;
	} else {
		mmap[12].addr_start = (uint32_t)(trambar & ~7u) << 8;
		mmap[12].addr_end = mmap[12].addr_start + 2048;
		mmap[12].readable = true;
		mmap[12].writeable = true;
		mmap[12].valid = true;
	}
}

static bool was_attached = false;	// was reset once in target lifetime
static bool is_attached = false;	// currently attached

static void on_first_attach() {
	supervisor_reset();
}

static void on_attach() {
	is_attached = true;
	digitalWrite(pin_led, LOW);
	if (!was_attached) {
		was_attached = true;
		on_first_attach();
	}
}

static void on_detach() {
	supervisor_leave();
	digitalWrite(pin_led, HIGH);
	is_attached = false;
}

static uint16_t measure_voltage(byte pin) {
	return (uint16_t)analogRead(pin) * 7u;
}

static void monitor_status() {
	static bool ser_connected_prev = false;
	const bool ser_connected = Serial;
	if (ser_connected_prev != ser_connected) {
		ser_connected_prev = ser_connected;
		if (ser_connected) {
			Serial.println(F("BDMdongle"));
			digitalWrite(pin_led, !target_is_frozen());
			mode_bin = false;
		}
	}

	const bool new_vdd = measure_voltage(pin_vsw) > 4500u;
	if (have_vdd != new_vdd) {
		have_vdd = new_vdd;
		is_attached = false;
		was_attached = false;
		digitalWrite(pin_led, HIGH);
		digitalWrite(pin_step, LOW);
	} else if (have_vdd && !is_attached && target_is_frozen()) {
		// in case target was already frozen, try to sync with it
		bdm_clrerror();
		on_attach();
	}

	if (!have_vdd || !ser_connected) {
		static unsigned long t_prev;
		static bool led_on;
		const unsigned long t_now = millis();
		unsigned long t_delay;

		if (!ser_connected) {
			if (led_on) {
				t_delay = 50;
			} else {
				t_delay = 850;
			}
		} else {
			t_delay = 200;
		}

		if (t_now - t_prev >= t_delay) {
			digitalWrite(pin_led, led_on);	// note: this is actually inverted
			led_on = !led_on;
			t_prev = t_now;
		}
	}
}

extern const BDMCommand commands[];

static void cmd_help() {
	Serial.println(F("Help:"));
	for (auto cmd : IterFlash<BDMCommand>(commands)) {
		Serial.print(' ');
		Serial.print(cmd.name);
		if (cmd.args) {
			Serial.print(reinterpret_cast<const __FlashStringHelper *>(cmd.args));
		}
		Serial.print(F(" - "));
		Serial.println(reinterpret_cast<const __FlashStringHelper *>(cmd.desc));
	}
}

static void cmd_status() {
	Serial.print(F("Target:"));
	if (target_has_vdd()) Serial.print(F(" VDD"));
	if (target_is_reset()) Serial.print(F(" RST"));
	if (target_is_frozen()) Serial.print(F(" FROZEN"));
	if (was_vsw_fault) Serial.print(F(" VSW_FAULT"));
	was_vsw_fault = false;
	Serial.println();
}

static void cmd_meas_pin(byte pin) {
	Serial.print(measure_voltage(pin));
	Serial.println(F("mV"));
}

static void cmd_meas_vbus() {
	cmd_meas_pin(pin_vbus);
}

static void cmd_meas_vsw() {
	cmd_meas_pin(pin_vsw);
}

static void cmd_vsw_on() {
	if (digitalRead(pin_req_vsw)) {
		Serial.println(F("OK"));
		return;
	}
	if (target_has_vdd()) {
		Serial.println(F("E: target already has supply"));
		return;
	}

	digitalWrite(pin_req_vsw, HIGH);
	Serial.println(F("OK"));
}

static void cmd_vsw_off() {
	digitalWrite(pin_req_vsw, LOW);
	Serial.println(F("OK"));
}

static void cmd_binary() {
	mode_bin = !mode_bin;
	Serial.println(mode_bin ? F("BIN") : F("ASCII"));
}

enum class WaitFreezeResp {
	Frozen,
	Aborted,
	LostVDD,
	Timeout,
};

static WaitFreezeResp wait_freeze(unsigned long timeout = 0) {
	const unsigned long ts_start = millis();

	while (!target_is_frozen()) {
		if (proto.read_abort()) {
			return WaitFreezeResp::Aborted;
		}
		monitor_status();
		if (!target_has_vdd()) {
			return WaitFreezeResp::LostVDD;
		} else if (timeout > 0 && (millis() - ts_start > timeout)) {
			return WaitFreezeResp::Timeout;
		}
	}
	if (!is_attached) {
		on_attach();
	}
	return WaitFreezeResp::Frozen;
}

static void send_response(WaitFreezeResp resp) {
	switch (resp) {
		case WaitFreezeResp::Frozen:
			Serial.println(F("OK"));
			return;
		case WaitFreezeResp::Aborted:
			Serial.println(F("ABORT"));
			return;
		case WaitFreezeResp::LostVDD:
			Serial.println(F("E: target lost power"));
			return;
		case WaitFreezeResp::Timeout:
			Serial.println(F("E: attach timed out"));
			return;
	}
	Serial.println(F("E: internal error"));
}

static void cmd_reset(bool attach) {
	if (target_is_frozen()) {
		on_detach();
	}
	was_attached = false;
	digitalWrite(pin_step, attach);
	digitalWrite(pin_rst, LOW);
	pinMode(pin_rst, OUTPUT);
	delay(100);
	pinMode(pin_rst, INPUT);
	if (attach) {
		auto resp = wait_freeze(100);
		if (resp == WaitFreezeResp::Timeout) {
			// CPU needs snap out of it
			// (i.e. if flash is empty CPU will hang)
			digitalWrite(pin_berr, LOW);
			pinMode(pin_berr, OUTPUT);
			pinMode(pin_berr, INPUT);
			resp = wait_freeze(10);
		}
		send_response(resp);
	} else {
		Serial.println(F("OK"));
	}
}

static void cmd_reset_run() {
	cmd_reset(false);
}

static void cmd_reset_att() {
	cmd_reset(true);
}

static void cmd_attach() {
	digitalWrite(pin_step, HIGH);
	auto resp = wait_freeze(100);
	if (resp != WaitFreezeResp::Frozen) {
		digitalWrite(pin_step, LOW);
	}
	send_response(resp);
}

static void target_go() {
	on_detach();
	digitalWrite(pin_step, LOW);
	send_cmd(BDM_CMD_GO);
}

static void cmd_go() {
	target_go();
	Serial.println(F("OK"));
}

static void cmd_go_and_wait() {
	if (target_is_frozen()) {
		target_go();
	}
	send_response(wait_freeze());
}

static WaitFreezeResp target_step() {
	on_detach();
	digitalWrite(pin_step, LOW);
	digitalWrite(pin_step, HIGH);
	send_cmd(BDM_CMD_GO);
	return wait_freeze();
}

static void cmd_step() {
	send_response(target_step());
}

static void cmd_until() {
	const auto addr = cmd_args.get<uint32_t>();

	for (;;) {
		uint32_t pc;

		if (!bdm_request(BDM_CMD_RSREG | BDM_REG_RPC, pc)) {
			Serial.println(F("E: RPC read failed"));
			return;
		}
		if (pc == addr) {
			break;
		}

		auto resp = target_step();
		if (resp != WaitFreezeResp::Frozen) {
			send_response(resp);
			return;
		}
		// Note: target_step() checks for abort,
		//       but in single-step mode target CPU is often too fast for the busy loop
		//       which exits immediately and doesn't get chance to check for abort.
		//       Therefore, there is another check.
		if (proto.read_abort()) {
			Serial.println(F("ABORT"));
			return;
		}
	}
	Serial.println(F("OK"));
}

static void cmd_test() {
	const uint32_t ret = send_cmd(BDM_CMD_NOP);
	dump_one_ret(ret);
}

static void cmd_read() {
	const auto szflags = cmd_args.get<uint16_t>();
	const auto addr = cmd_args.get<uint32_t>();
	uint32_t ret;

	if (!mem_read(ret, addr, szflags)) {
		Serial.println(F("E: read failed"));
		return;
	}

	if (mode_bin) {
		Serial.println(F("BIN"));
	} else {
		Serial.print('@');
		Serial.print(addr, HEX);
		Serial.print(':');
	}
	dump_ret(ret, szflags);
}

static void cmd_write() {
	const auto szflags = cmd_args.get<uint16_t>();
	const auto addr = cmd_args.get<uint32_t>();
	const auto data = cmd_args.get<uint32_t>();

	if (!mem_write(data, addr, szflags)) {
		Serial.println(F("E: write failed"));
		return;
	}
	Serial.println(F("OK"));
}

static void cmd_dump() {
	const auto szflags = cmd_args.get<uint16_t>();
	auto count = cmd_args.get<uint32_t>();
	if (count < 1) count = 1;

	bool first = true;
	while (count--) {
		uint32_t ret;
		if (!bdm_request(BDM_CMD_DUMP | szflags, ret)) {
			Serial.println(F("E: read failed"));
			break;
		}
		if (mode_bin && first) {
			Serial.println(F("BIN"));
		}
		dump_ret(ret, szflags);
		first = false;
	}
}

static void cmd_fill() {
	const auto szflags = cmd_args.get<uint16_t>();
	auto count = cmd_args.get<uint32_t>();
	if (count < 1) {
		Serial.println(F("OK"));
		return;
	}

	if (mode_bin) {
		Serial.println(F("BIN"));
	}
	while (count--) {
		const uint32_t val = proto.read_abu32(szflags);
		if (!(szflags == BDM_SIZE_LONG && bdm_request_l(BDM_CMD_FILL | szflags, val)) &&
		    !(szflags != BDM_SIZE_LONG && bdm_request_w(BDM_CMD_FILL | szflags, val))) {
			Serial.println(F("E: write failed"));
			return;
		}
	}
	Serial.println(F("OK"));
}

static const BDMRegister registers[] PROGMEM = {
	{"D0", false,  0},
	{"D1", false,  1},
	{"D2", false,  2},
	{"D3", false,  3},
	{"D4", false,  4},
	{"D5", false,  5},
	{"D6", false,  6},
	{"D7", false,  7},
	{"A0", false,  8},
	{"A1", false,  9},
	{"A2", false, 10},
	{"A3", false, 11},
	{"A4", false, 12},
	{"A5", false, 13},
	{"A6", false, 14},
	{"A7", false, 15},
	{"RPC", true, BDM_REG_RPC},
	{"PCC", true, BDM_REG_PCC},
	{"SR" , true, BDM_REG_SR},
	{"USP", true, BDM_REG_USP},
	{"SSP", true, BDM_REG_SSP},
	{"SFC", true, BDM_REG_SFC},
	{"DFC", true, BDM_REG_DFC},
	{"TMP", true, BDM_REG_ATEMP},
	{"FAR", true, BDM_REG_FAR},
	{"VBR", true, BDM_REG_VBR},
	{0}
};

BDMRegister BDMRegister::find(const char *name) {
	for (auto reg : IterFlash<BDMRegister>(registers)) {
		if (!strcmp(reg.name, name))
			return reg;
	}
	return {};
}

bool BDMRegister::read(uint32_t &val) const {
	const uint16_t cmd = (is_sys ? BDM_CMD_RSREG : BDM_CMD_RDREG) | id;
	return is_valid() && bdm_request(cmd, val);
}

bool BDMRegister::write(uint32_t val) {
	const uint16_t cmd = (is_sys ? BDM_CMD_WSREG : BDM_CMD_WRREG) | id;
	return is_valid() && bdm_request_l(cmd, val);
}

static void cmd_reg_rd() {
	auto reg = cmd_args.get<BDMRegister>();
	uint32_t ret;

	if (!reg.is_valid()) {
		Serial.println(F("E: unknown register"));
		return;
	}

	if (!reg.read(ret)) {
		Serial.println(F("E: read failed"));
		return;
	}

	dump_one_ret(ret);
}

static void cmd_reg_wr() {
	auto reg = cmd_args.get<BDMRegister>();
	const auto val = cmd_args.get<uint32_t>();

	if (!reg.is_valid()) {
		Serial.println(F("E: unknown register"));
		return;
	}

	if (!reg.write(val)) {
		Serial.println(F("E: write failed"));
		return;
	}

	Serial.println(F("OK"));
}

static void cmd_reg_multi_rd() {
	const auto set = cmd_args.get<char>();

	if (mode_bin) {
		Serial.println(F("BIN"));
	}
	for (auto reg : IterFlash<BDMRegister>(registers)) {
		if (!reg.in_set(set))
			continue;

		if (!mode_bin) {
			Serial.print(reg.name);
			Serial.print('=');
		}
		uint32_t val;
		if (!reg.read(val)) {
			Serial.println(F("E: read failed"));
		}
		dump_ret(val);
	}
}

static void cmd_reg_multi_wr() {
	const auto set = cmd_args.get<char>();

	if (mode_bin) {
		Serial.println(F("BIN"));
	}
	for (auto reg : IterFlash<BDMRegister>(registers)) {
		if (!reg.in_set(set))
			continue;

		if (!mode_bin) {
			Serial.print(reg.name);
			Serial.println(F("=?"));
		}
		if (!reg.write(proto.read_abu32())) {
			Serial.println(F("E: write failed"));
			return;
		}
	}
	Serial.println(F("OK"));
}

static void cmd_show_mm() {
	supervisor_enable();

	for (uint8_t csid = 0; csid < 13; ++csid) {
		const auto &marea = mmap[csid];
		if (marea.valid) {
			Serial.print(csid);
			Serial.print(',');
			Serial.print(marea.addr_start, HEX);
			Serial.print(',');
			Serial.print(marea.addr_end, HEX);
			Serial.print(',');
			if (marea.readable && marea.writeable) {
				Serial.print(F("rw"));
			} else if (marea.readable) {
				Serial.print(F("ro"));
			} else {
				Serial.print(F("wo"));
			}
			Serial.println();
		}
	}
	Serial.println(F("OK"));
}

static constexpr uint8_t seq_max_size = 16;
static struct {
	uint32_t addr;
	uint16_t szflags;
	uint32_t data;
} seq[seq_max_size];
static uint8_t seq_size = 0;

static void cmd_seq_set() {
	const auto id = cmd_args.get<uint32_t>();
	const auto addr = cmd_args.get<uint32_t>();
	const auto szflags = cmd_args.get<uint16_t>();
	const auto data = cmd_args.get<uint32_t>();

	if ((
	      ((szflags == BDM_SIZE_WORD) && (data > 0xFFFF)) ||
	      ((szflags == BDM_SIZE_BYTE) && (data > 0xFF))
	    ) && (id <= seq_max_size)) {
		seq_size = id;
		Serial.println(F("OK"));
	} else if (id < seq_max_size) {
		seq[id].addr = addr;
		seq[id].szflags = szflags;
		seq[id].data = data;
		Serial.println(F("OK"));
	} else {
		Serial.println(F("E: invalid ID"));
	}
}

static bool _cmd_seq_exec(uint8_t id, uint32_t addr, uint32_t data) {
	for (uint8_t i = 0; i < seq_size; ++i) {
		uint32_t wr_addr, wr_data;
		if (i == id) {
			wr_addr = addr;
			wr_data = data;
		} else {
			wr_addr = seq[i].addr;
			wr_data = seq[i].data;
		}
		if (!mem_write(wr_data, wr_addr, seq[i].szflags)) {
			return false;
		}
	}
	return true;
}

class Buffer {
	public:
		using bsize_t = uint8_t;

		bool resize(uint16_t szflags, uint32_t count) {
			uint32_t real_size = count;
			if (szflags & BDM_SIZE_WORD) {
				real_size *= 2;
			} else if (szflags & BDM_SIZE_LONG) {
				real_size *= 4;
			}
			if (real_size > max_bytes) {
				return false;
			}

			len = (bsize_t)count;
			return true;
		}

		bool load(uint16_t szflags, uint32_t count) {
			if (!resize(szflags, count)) {
				Serial.println(F("E: data too large"));
				return false;
			}

			if (mode_bin) {
				Serial.println(F("BIN"));
			}
			for (bsize_t i = 0; i < len; ++i) {
				set(i, proto.read_abu32(szflags));
			}
			return true;
		}

		bool set(bsize_t i, uint32_t val) {
			if (i >= len)
				return false;
			if (szflags & BDM_SIZE_WORD) {
				ref<uint16_t>(i) = (uint16_t)val;
			} else if (szflags & BDM_SIZE_LONG) {
				ref<uint32_t>(i) = val;
			} else {
				ref<uint8_t>(i) = (uint8_t)val;
			}
			return true;
		}

		uint32_t get(bsize_t i) {
			if (i >= len)
				return 0;
			if (szflags & BDM_SIZE_WORD) {
				return ref<uint16_t>(i);
			} else if (szflags & BDM_SIZE_LONG) {
				return ref<uint32_t>(i);
			} else {
				return ref<uint8_t>(i);
			}
		}

		bsize_t size() const {
			return len;
		}

	private:
		static constexpr bsize_t max_bytes = 128;
		uint8_t data[max_bytes] = {0};
		uint16_t szflags = 0;
		bsize_t len = 0;

		template<typename T>
		T& ref(bsize_t i) {
			return ((T*)data)[i];
		}
} buffer;

static void cmd_seq_exec() {
	const auto id = cmd_args.get<uint32_t>();
	auto addr = cmd_args.get<uint32_t>();
	const char inc = cmd_args.get<char>();
	auto count = cmd_args.get<uint32_t>();

	if (id < seq_size) {
		if (mode_bin) {
			Serial.println(F("BIN"));
		}
		while (count--) {
			const uint32_t val = proto.read_abu32(seq[id].szflags);
			if (!_cmd_seq_exec(id, addr, val)) {
				Serial.println(F("E: write failed"));
				return;
			}
			if (inc == '+')
				++addr;
		}
	} else {
		for (uint32_t i = 0; i < count; ++i) {
			if (!_cmd_seq_exec(id, 0, 0)) {
				Serial.println(F("E: write failed"));
				return;
			}
		}
	}

	Serial.println(F("OK"));
}

static bool flash_wait(uint32_t addr) {
	uint32_t rd;
	uint8_t prev;

	if (!mem_read(rd, addr, BDM_SIZE_BYTE))
		return false;
	prev = (uint8_t)rd;

	for (;;) {
		if (!mem_read(rd, addr, BDM_SIZE_BYTE)) {
			return false;
		}
		uint8_t cur = (uint8_t)rd;
		if (prev == cur) {
			return true;
		} else if (prev & (1 << 5)) {
			return false;
		}
	}
}

static void cmd_seq_flash_wr() {
	const uint8_t id = cmd_args.get<uint32_t>();
	auto addr = cmd_args.get<uint32_t>();
	const auto len = cmd_args.get<uint32_t>();

	// load to buffer
	if (id >= seq_size) {
		Serial.println("E: invalid seq id");
		return;
	}
	if (!buffer.load(seq[id].szflags, len)) {
		// load will send error message
		return;
	}

	// flash
	for (Buffer::bsize_t i = 0; i < buffer.size(); ++i, ++addr) {
		if (_cmd_seq_exec(id, addr, buffer.get(i))) {
			if (!flash_wait(addr)) {
				Serial.println(F("E: timeout"));
				return;
			}
		} else {
			Serial.println(F("E: seq exec failed"));
			return;
		}
	}
	Serial.println(F("OK"));
}

static void cmd_flash_wait() {
	const auto addr = cmd_args.get<uint32_t>();
	if (flash_wait(addr)) {
		Serial.println(F("OK"));
	} else {
		Serial.println(F("E: timeout"));
	}
}

static void cmd_store() {
	digitalWrite(pin_flash_cs, LOW);
	SPI.transfer(0x90);
	SPI.transfer(0x00);
	SPI.transfer(0x00);
	SPI.transfer(0x00);
	byte id = SPI.transfer(0x00);
	digitalWrite(pin_flash_cs, HIGH);
	Serial.print(F("Flash ID:"));
	Serial.println(id, HEX);
}


#define CMD(name, longname, allowed_mode) \
	{name, allowed_mode, cmd_##longname, cmd_format_##longname, cmd_args_##longname, cmd_desc_##longname}
#define CMD_DESC(longname, desc) \
	static constexpr const char *cmd_format_##longname = nullptr; \
	static constexpr const char *cmd_args_##longname = nullptr; \
	static const char cmd_desc_##longname[] PROGMEM = desc
#define CMD_DESA(longname, fmt, args, desc) \
	static const char cmd_format_##longname[] PROGMEM = fmt; \
	static const char cmd_args_##longname[] PROGMEM = args; \
	static const char cmd_desc_##longname[] PROGMEM = desc
CMD_DESC(help, "this help");
CMD_DESC(status, "show status");
CMD_DESC(meas_vbus, "measure USB voltage");
CMD_DESC(meas_vsw, "measure target voltage");
CMD_DESC(vsw_on, "turn on target power");
CMD_DESC(vsw_off, "turn off target power");
CMD_DESC(binary, "toggle binary/ascii mode");
CMD_DESC(reset_run, "reset target and run");
CMD_DESC(reset_att, "reset target and debug");
CMD_DESC(attach, "start debugging");
CMD_DESC(go, "resume target");
CMD_DESC(go_and_wait, "resume target and/or wait for freeze");
CMD_DESC(step, "step target");
CMD_DESA(until, "u", "<ADDR>", "step until specific PC");
CMD_DESC(test, "execute NOP");
CMD_DESA(read, "wu", "[bwl]<ADDR>", "read memory");
CMD_DESA(write, "wuu", "[bwl]<ADDR><DATA>", "write memory");
CMD_DESA(dump, "wu", "[bwl]<COUNT>", "continue reading");
CMD_DESA(fill, "wu", "[bwl]<COUNT>", "continue writing");
CMD_DESA(reg_rd, "R", "<NAME>", "read register");
CMD_DESA(reg_wr, "Ru", "<NAME><val>", "write register");
CMD_DESA(reg_multi_rd, "c", "[ag]", "read multiple registers");
CMD_DESA(reg_multi_wr, "c", "[ag]", "write multiple registers");
CMD_DESC(show_mm, "show memory map");
CMD_DESA(seq_set, "uuwu", "<ID><ADDR>[bwl]<DATA>", "store sequence; invalid value defines length");
CMD_DESA(seq_exec, "uucu", "<ID><ADDR>[+=]<LEN>", "exec sequencer with replaced id for multiple data");
CMD_DESA(seq_flash_wr, "uuu", "<ID><ADDR><LEN>", "exec sequencer and poll for each data");
CMD_DESA(flash_wait, "u", "<ADDR>", "wait for flash completion");
CMD_DESC(store, "store program to internal flash");
static const BDMCommand commands[] PROGMEM = {
	CMD('h', help, CM_ANY),
	CMD('t', status, CM_ANY),
	CMD('v', meas_vbus, CM_ANY),
	CMD('V', meas_vsw, CM_ANY),
	CMD('W', vsw_on, CM_ANY),
	CMD('w', vsw_off, CM_ANY),
	CMD('b', binary, CM_ANY),
	CMD('r', reset_run, CM_OPERABLE),
	CMD('R', reset_att, CM_OPERABLE),
	CMD('a', attach, CM_OPERABLE),
	CMD('g', go, CM_DBG),
	CMD('G', go_and_wait, CM_OPERABLE),
	CMD('s', step, CM_DBG),
	CMD('u', until, CM_DBG),
	CMD('T', test, CM_DBG),
	CMD('@', read, CM_DBG),
	CMD('=', write, CM_DBG),
	CMD('d', dump, CM_DBG),
	CMD('D', fill, CM_DBG),
	CMD('p', reg_rd, CM_DBG),
	CMD('P', reg_wr, CM_DBG),
	CMD('e', reg_multi_rd, CM_DBG),
	CMD('E', reg_multi_wr, CM_DBG),
	CMD('m', show_mm, CM_DBG),
	CMD('q', seq_set, CM_ANY),
	CMD('Q', seq_exec, CM_DBG),
	CMD('F', seq_flash_wr, CM_DBG),
	CMD('f', flash_wait, CM_DBG),
	CMD('S', store, CM_ANY),
	{0}
};

const BDMCommand find_command(char name) {
	for (auto cmd : IterFlash<BDMCommand>(commands)) {
		if (cmd.name == name)
			return cmd;
	}
	return {};
}

void loop() {
	monitor_status();

	int req = Serial.read();
	if (req >= 0 && req != ' ' && req != '\r' && req != '\n') {
		const bool operable = have_vdd && !target_is_reset();
		const BDMCommand cmd = find_command(req);

		if (!cmd.name) {
			cmd_args.flush();
			Serial.println(F("E: unknown command"));
		} else if (
		           (!(cmd.allowed_mode & CM_OPERABLE) || operable) &&
		           (!(cmd.allowed_mode & CM_FROZEN)   || target_is_frozen())
		          ) {
			if (cmd_args.parse(cmd.format)) {
				cmd.handler();
			} else {
				Serial.println(F("E: parser error"));
			}
		} else {
			cmd_args.flush();
			Serial.println(F("E: not ready"));
		}
	}
}
