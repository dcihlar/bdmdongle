# BDMdongle

BDMdongle is an open source tool for debugging CPU32 platforms.

Arduino firmware implements all low level operations and communicates
with a Python tool via serial.

## Dongle

![Dongle](hw/BDMdongle.jpg)

This custom made hardware (JLCPCB assembly ready) interfaces CPU32 BDM interface,
and implements many low-level commands in firmware.

It can be supplied either by USB or target,
and it can also supply target from USB with some basic current flow protection.

### Protocol

The protocol used by dongle is human friendly.
Just connect to it using a serial terminal like CuteCom and send 'h\n' to see all available commands.

But it is not usable for anything complex. Use the `bdm` tool instead.

## Tool

Use `--help` for help.

### gdb

`bdm` can act as a gdb server.

When debugging applications running from RAM there is no limit for the number of breakpoints.
But applications running from ROM can only have a single breakpoint,
and are run instruction-by-instruction by the dongle until `PC` is matched which makes it much slower.

Supported monitor commands are:

 * `reset` - resets and halts the target on the first instruction

In some cases some additional hardware initialization before running gdbserver may be required.
I.e. chip select registers should be initialized before loading an application into RAM.
Or maybe `SP` needs to be initialized or watchdog disabled.
Either `bdm cli -f` or gdb init script can be used for that.
