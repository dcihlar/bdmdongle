#!/usr/bin/env python3

from distutils.core import setup

setup(
	name='BDMdongle',
	version='1.0',
	description='CPU32 development tool',
	author='Davor Cihlar',
	author_email='davor@cihlar.biz',
	url='https://bitbucket.org/dcihlar/bdmdongle/src/master/',
	packages=['bdmdongle'],
	install_requires=['intelhex', 'pyelftools'],
	scripts=['bdm'],
)
