from elftools.elf.elffile import ELFFile


class Firmware():
	def __init__(self, fwfile):
		self.sections = {}
		self.zeros = {}
		self.entry = None

		# load elf
		elf = ELFFile(fwfile)

		# validate architecture
		if elf.get_machine_arch() != 'Motorola 68000':
			raise ValueError('invalid architecture')

		# get entry point
		self.set_entry(elf['e_entry'])

		# load data
		for seg in filter(lambda seg: seg['p_type'] == 'PT_LOAD', elf.iter_segments()):
			if seg['p_filesz']:
				self.add_section(seg['p_paddr'], seg.data())
			numzeros = seg['p_memsz'] - seg['p_filesz']
			if numzeros > 0:
				self.add_zeros(seg['p_paddr'] + seg['p_filesz'], numzeros)

	def add_section(self, addr, data):
		self.sections[addr] = data

	def add_zeros(self, addr, count):
		self.zeros[addr] = count

	def set_entry(self, entry):
		self.entry = entry
