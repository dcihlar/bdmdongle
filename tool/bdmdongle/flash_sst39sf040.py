from bdmdongle.flash import Flash, FlashInfo, FlashError


class Flash_MX29F040C(Flash):
	_seq_config = [
		(0x5555, 'b', 0xAA),	# unlock 1
		(0x2AAA, 'b', 0x55),	# unlock 2
		(0x5555, 'b', 0xA0),	# cmd
		(0x0000, 'b', 0x00),	# data
	]
	_seq_data_ofs = 3

	def _read_byte(self, addr):
		return self.dongle.read_mem(self.base_address + addr, 'b')[0]

	def _write_byte(self, addr, val):
		self.dongle.write_mem(self.base_address + addr, 'b', [val])

	def _send_cmd(self, cmd, sa=0x5555):
		self._write_byte(0x5555, 0xAA)
		self._write_byte(0x2AAA, 0x55)
		self._write_byte(sa    , cmd )

	def _reset(self):
		self._write_byte(0, 0xF0)

	def _wait_finish(self):
		prev = self._read_byte(0)
		while True:
			now = self._read_byte(0)
			if prev == now:
				break
			elif prev & (1<<5):
				raise FlashError('operation timeout')
			prev = now

	def probe(self):
		self.dongle.configure_sequencer(self._seq_config)
		self._send_cmd(0x90)
		mdid = self._read_byte(0), self._read_byte(1)
		self._reset()
		if   mdid == (0xBF, 0xB5):
			info = FlashInfo(4*1024, 128*1024, 'SST39SF010A')
		elif mdid == (0xBF, 0xB6):
			info = FlashInfo(4*1024, 256*1024, 'SST39SF020A')
		elif mdid == (0xBF, 0xB7):
			info = FlashInfo(4*1024, 512*1024, 'SST39SF040')
		else:
			raise FlashError('flash chip not recognized: %02X:%02X' % mdid)
		return info

	def erase_page(self, addr):
		self._validate_addr(addr, 'erase page')
		self._send_cmd(0x80)
		self._send_cmd(0x30, sa=addr)
		self._wait_finish()

	def erase_chip(self):
		self._send_cmd(0x80)
		self._send_cmd(0x10)
		self._wait_finish()

	def write_page(self, addr, data):
		self._validate_addr(addr, 'write page', aligned=False)
		self._validate_addr(addr + len(data), 'write page end', aligned=False)
		self.dongle.sequencer_flash(self._seq_data_ofs, addr, data)
