# Note: this is inspired from OllyDBG2 source
#       and they also say:
#			Code a bit inspired from http://mspgcc.cvs.sourceforge.net/viewvc/mspgcc/msp430simu/gdbserver.py?revision=1.3&content-type=text%2Fplain

import socketserver
import logging
import socket
import struct
import errno
import sys

import bdmdongle


mmxml = '''<?xml version="1.0"?>
<!DOCTYPE memory-map
          PUBLIC "+//IDN gnu.org//DTD GDB Memory Map V1.0//EN"
                 "http://sourceware.org/gdb/gdb-memory-map.dtd">
<memory-map>
<memory type="rom" start="0" length="524288"/>
<memory type="ram" start="1048576" length="262144"/>
<memory type="ram" start="16775680" length="1536"/>
</memory-map>
'''


GDB_SIGNAL_TRAP = 5

def checksum(data):
	checksum = 0
	for c in data:
		checksum += ord(c)
	return checksum & 0xff

class HandlerError(Exception):
	def __init__(self, code):
		super(HandlerError, self).__init__('gdb command execution failed')
		self.code = code

class GDBServerHandler(socketserver.StreamRequestHandler):
	def __init__(self, dongle, *args, **kwargs):
		self.log = logging.getLogger('gdbserver')
		self.dongle = dongle
		# in gdb order
		self.reg_names = tuple('D%d' % i for i in range(8)) + \
		                 tuple('A%d' % i for i in range(8)) + \
		                 ('SR', 'RPC')
		super(GDBServerHandler, self).__init__(*args, **kwargs)

	def setup(self):
		super(GDBServerHandler, self).setup()
		self.last_pkt = None
		self._hw_breakpoint = None
		self._sw_breakpoints = {}
		self.log.info('connected')
		self.dongle.attach()	# make sure we're in debug mode

	def finish(self):
		try:
			for addr in list(self._sw_breakpoints.keys()):
				self.sw_breakpoint_clear(addr)
		except HandlerError:
			self.log.warning('couldn\'t clean up breakpoints - the code is left damaged')
		super(GDBServerHandler, self).finish()
		self.dongle.poll_abort = self._orig_poll_abort
		self.log.info('closed')

	def hw_breakpoint_set(self, addr):
		if self._hw_breakpoint is not None:
			return False
		self._hw_breakpoint = addr
		return True

	def hw_breakpoint_clear(self):
		self._hw_breakpoint = None

	def sw_breakpoint_set(self, addr):
		if addr in self._sw_breakpoints:
			return
		try:
			self._sw_breakpoints[addr] = self.dongle.read_mem_blob(addr, 2)
			self.dongle.write_mem_blob(addr, b'\x4a\xfa')
		except bdmdongle.CommandError:
			if addr in self._sw_breakpoints:
				del self._sw_breakpoints[addr]
			raise HandlerError(errno.EFAULT)

	def sw_breakpoint_clear(self, addr):
		if addr not in self._sw_breakpoints:
			return False
		try:
			self.dongle.write_mem_blob(addr, self._sw_breakpoints[addr])
		except bdmdongle.CommandError:
			raise HandlerError(errno.EFAULT)
		del self._sw_breakpoints[addr]
		return True

	def handle_xfer(self, subcmd):
		args = subcmd.split(':')
		if args[0] == 'memory-map' and args[1] == 'read':
			self.log.info('Received Xfer:memory-map')
			addr, size = args[3].split(',')
			addr = int(addr, 16)
			size = int(size, 16)
			chunk = mmxml[addr:addr+size]
			return ('m' if len(chunk) == size else 'l') + chunk
		else:
			self.log.error('Unknown Xfer command: %s' % args[0])

	def handle_rcmd(self, subcmd):
		subcmd = bytes.fromhex(subcmd).decode()
		if subcmd == 'reset':
			self.log.info('Reset traget')
			self.dongle.reset(attach=True)
			return True
		else:
			self.log.error('Unknown Rcmd "%s"' % subcmd)

	def handle_q(self, cmd, subcmd):
		# https://sourceware.org/gdb/onlinedocs/gdb/General-Query-Packets.html
		if subcmd.startswith('Supported'):
			# subcmd Supported: https://sourceware.org/gdb/onlinedocs/gdb/General-Query-Packets.html#qSupported
			# Report the features supported by the RSP server. As a minimum, just the packet size can be reported.
			self.log.info('Received qSupported command')
			return 'PacketSize=%x;qXfer:memory-map:read+' % 4096
		elif subcmd.startswith('Attached'):
			self.log.info('Received qAttached command')
			return '1'
		elif subcmd.startswith('Xfer:'):
			return self.handle_xfer(subcmd[5:])
		elif subcmd.startswith('Rcmd,'):
			return self.handle_rcmd(subcmd[5:])
		else:
			self.log.error('This subcommand %r is not implemented in q' % subcmd)

	def handle_qmark(self, cmd, subcmd):
		return 'S%.2x' % GDB_SIGNAL_TRAP

	def handle_g(self, cmd, subcmd):
		self.log.info('Received a "read all registers" command')
		if subcmd == '':
			registers = self.dongle.read_multiple_regs('g')
			return ''.join(struct.pack('>I', registers[name]).hex() for name in self.reg_names)

	def handle_G(self, cmd, subcmd):
		self.log.info('Received a "set all registers" command')
		reg_name = iter(self.reg_names)
		registers = {}
		while subcmd:
			val, subcmd = subcmd[:8], subcmd[8:]
			val = struct.unpack('>I', bytes.fromhex(val))[0]
			registers[next(reg_name)] = val
		self.dongle.write_multiple_regs(registers, 'g')
		return True

	def handle_p(self, cmd, subcmd):
		regid = int(subcmd, 16)
		if regid not in range(len(self.reg_names)):
			self.log.error('Accessing non-existent register')
			return
		regname = self.reg_names[regid]
		self.log.info('Received a "read register %s" command' % regname)
		return struct.pack('>I', self.dongle.read_reg(regname)).hex()

	def handle_P(self, cmd, subcmd):
		regid, val = subcmd.split('=')
		regid = int(regid, 16)
		val = int(val, 16)
		if regid not in range(len(self.reg_names)):
			self.log.error('Accessing non-existent register')
			return
		regname = self.reg_names[regid]
		self.log.info('Set register %s=0x%x' % (regname, val))
		self.dongle.write_reg(regname, val)
		return True

	def handle_m(self, cmd, subcmd):
		addr, size = subcmd.split(',')
		addr = int(addr, 16)
		size = int(size, 16)
		self.log.info('Received a "read memory" command (@%#.8x : %d bytes)' % (addr, size))
		try:
			return self.dongle.read_mem_blob(addr, size).hex()
		except bdmdongle.CommandError:
			raise HandlerError(errno.EFAULT)

	def handle_M(self, cmd, subcmd):
		addr, sizedata = subcmd.split(',')
		size, data = sizedata.split(':')
		addr = int(addr, 16)
		size = int(size, 16)
		try:
			self.dongle.write_mem_blob(addr, bytes.fromhex(data))
		except bdmdongle.CommandError:
			raise HandlerError(errno.EFAULT)
		return True

	def handle_s(self, cmd, subcmd):
		self.log.info('Received a "single step" command')
		self.dongle.step_instruction()
		return 'T%.2x' % GDB_SIGNAL_TRAP

	def handle_z(self, cmd, subcmd):
		typ, addr, kind = subcmd.split(',', 3)
		typ = int(typ)
		addr = int(addr, 16)
		if typ == 0:
			if self.sw_breakpoint_clear(addr):
				self.log.info('Removed software breakpoint')
				return True
			else:
				self.log.error('Failed removing software breakpoint')
				raise HandlerError(errno.EINVAL)
		elif typ == 1:
			self.log.info('Remove hardware breakpoint')
			self.hw_breakpoint_clear()
			return True
		else:
			self.log.error('z%d not supported' % typ)

	def handle_Z(self, cmd, subcmd):
		typ, addr, kind = subcmd.split(',', 3)
		typ = int(typ)
		addr = int(addr, 16)
		if typ == 0 and kind == '2':
			self.log.info('Set software breakpoint to 0x%x' % addr)
			self.sw_breakpoint_set(addr)
			return True
		elif typ == 1 and kind == '2':
			if self.hw_breakpoint_set(addr):
				self.log.info('Set hardware breakpoint to 0x%x' % addr)
				return True
			else:
				self.log.error('Too many breakpoints')
				raise HandlerError(errno.ENOMEM)
		else:
			self.log.error('Z%d not supported' % typ)

	def handle_c(self, cmd, subcmd):
		if subcmd == '':
			self.dongle.run(until=self._hw_breakpoint, wait=True)
			return 'T%.2x' % GDB_SIGNAL_TRAP
		else:
			self.log.error('Continue from address not supported')

	def handle_D(self, cmd, subcmd):
		self.log.info('gdb detaching')
		return True

	def handle_unknown(self, cmd, subcmd):
		self.log.info('"%s" command not handled' % cmd)

	def handle(self):
		'''Handle GDB command.
		   Commands are documented here: https://sourceware.org/gdb/current/onlinedocs/gdb/Remote-Protocol.html'''

		tr = {
			'?': 'qmark',
		}

		self._orig_poll_abort = self.dongle.poll_abort
		self.dongle.poll_abort = self.poll_abort

		while self.receive() == 'Good':
			pkt = self.last_pkt
			self.log.debug('receive(%r)' % pkt)
			# Each packet should be acknowledged with a single character. '+' to indicate satisfactory receipt
			self.send_raw('+')

			cmd, subcmd = pkt[0], pkt[1:]
			if cmd == 'k':
				break

			handler_name = 'handle_' + tr.get(cmd, cmd)
			if hasattr(self, handler_name):
				handler = getattr(self, handler_name)
			else:
				handler = self.handle_unknown

			try:
				resp = handler(cmd, subcmd)
			except HandlerError as e:
				resp = 'E%02x' % e.code
			except bdmdongle.CommandError as e:
				self.log.error('dongle command failed: ' + str(e))
				return
			except bdmdongle.SerialError as e:
				self.log.error('communication with dongle failed: ' + str(e))
				return
			if resp is None:
				resp = ''
			elif resp is True:
				resp = 'OK'
			self.send(resp)

	def poll_abort(self):
		self.connection.settimeout(0.)
		try:
			got_abort = b'\x03' in self.connection.recv(1, socket.MSG_PEEK)
		except BlockingIOError:
			got_abort = False
		self.connection.settimeout(None)
		if got_abort:
			b = b''
			while b != b'\x03':
				b = self.connection.recv(1)
		return got_abort

	def receive(self):
		'''Receive a packet from a GDB client'''
		# XXX: handle the escaping stuff '}' & (n^0x20)
		csum = 0
		state = 'Finding SOP'
		packet = ''
		while True:
			c = self.rfile.read(1).decode()
			if c == '\x03':
				return 'Error: CTRL+C'

			if len(c) != 1:
				return 'Error: EOF'

			if state == 'Finding SOP':
				if c == '$':
					state = 'Finding EOP'
			elif state == 'Finding EOP':
				if c == '#':
					if csum != int(self.rfile.read(2), 16):
						raise Exception('invalid checksum')
					self.last_pkt = packet
					return 'Good'
				else:
					packet += c
					csum = (csum + ord(c)) & 0xff
			else:
				raise Exception('should not be here')

	def send(self, msg):
		'''Send a packet to the GDB client'''
		self.log.debug('send(%r)' % msg)
		self.send_raw('$%s#%.2x' % (msg, checksum(msg)))

	def send_raw(self, r):
		self.wfile.write(r.encode())


def run(dongle, port=4242):
	def handler_maker(*args, **kwargs):
		return GDBServerHandler(dongle, *args, **kwargs)
	with socketserver.TCPServer(('localhost', port), handler_maker, bind_and_activate=False) as server:
		server.allow_reuse_address = True
		try:
			server.server_bind()
			server.server_activate()
		except:
			server.server_close()
			raise
		try:
			server.serve_forever()
		except KeyboardInterrupt:
			pass
