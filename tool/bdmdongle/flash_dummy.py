from bdmdongle.flash import Flash, FlashInfo


class Flash_Dummy(Flash):
	def __init__(self, *args, **kwargs):
		self._page_size = 64
		self._chip_size = 1024 * 1024
		self.erase_chip()
		super(Flash_Dummy, self).__init__(*args, **kwargs)

	def set_page_size(self, page_size):
		if page_size > self._chip_size:
			raise ValueError('page can\'t be larger than the whole chip')
		self._page_size = page_size
		if self.info is not None:
			self.info.page_size = page_size

	def set_chip_size(self, chip_size):
		delta = chip_size - self._chip_size
		self._chip_size = chip_size
		if self.info is not None:
			self.info.size = chip_size
		if delta > 0:
			self._data += bytearray([0xFF] * delta)
		else:
			self._data = self._data[0:chip_size]
		if self._page_size > chip_size:
			self.set_page_size(chip_size)

	def probe(self):
		return FlashInfo(self._page_size, len(self._data), 'Dummy')

	def erase_page(self, addr):
		addr -= addr % self._page_size
		self._data[addr:addr+self._page_size] = bytearray([0xFF] * self._page_size)

	def erase_chip(self):
		self._data = bytearray([0xFF] * self._chip_size)

	def read(self, addr, size):
		return self._data[addr:addr+size]

	def write_page(self, addr, data):
		for faddr, bdata in zip(range(addr, addr+len(data)), data):
			self._data[faddr] = bdata
