from abc import ABC, abstractmethod
import logging


class FlashError(Exception):
	pass

class FlashPluginError(Exception):
	pass


class FlashInfo():
	def __init__(self, page_size, size, name='noname'):
		self.page_size = page_size
		self.size = size
		self.name = name


class Flash(ABC):
	def __init__(self, dongle, base_address):
		self.log = logging.getLogger('flash')
		self.base_address = base_address
		self.dongle = dongle
		self.info = None

	@abstractmethod
	def probe(self):
		'''Identifies/checks flash IC
		   and regurns FlashInfo.
		   Raises FlashError for unknown flash or other error.
		'''
		pass

	@abstractmethod
	def erase_page(self, addr):
		pass

	@abstractmethod
	def erase_chip(self):
		pass

	@abstractmethod
	def write_page(self, addr, data):
		pass

	def init(self):
		try:
			self.info = self.probe()
		except FlashError:
			self.log.error('Couldn\'t identify flash.')
			raise
		return self.info

	def _validate_addr(self, addr, use, aligned=True):
		if addr not in range(0, self.info.size):
			raise FlashError('%s address out of range' % use)
		if aligned and addr % self.info.page_size:
			raise FlashError('%s address must be aligned to page address' % use)

	def erase(self, addr, size):
		self._validate_addr(addr, 'erase')
		if size % self.info.page_size:
			raise FlashError('erase size must be a multiple of page address')

		if size == self.info.size:
			self.log.info('Erasing chip')
			self.erase_chip(self)
			return

		for page_addr in range(addr, size, self.info.page_size):
			self.log.info('Erasing page 0x%x' % page_addr)
			self.erase_page(page_addr)

	def read(self, addr, size):
		return self.dongle.read_mem_blob(self.base_address + addr, size)

	def read_page(self, addr):
		self._validate_addr(addr, 'read')
		return self.read(addr, self.info.page_size)

	def write(self, addr, data, verify=True):
		def paginate():
			page_size = self.info.page_size
			rem_sz = page_size - addr % page_size
			end_addr = addr + len(data)
			end_aligned = end_addr - end_addr % page_size
			yield addr, data[0:rem_sz]
			for next_addr in range(addr + rem_sz, end_aligned, page_size):
				ofs = next_addr - addr
				yield next_addr, data[ofs:ofs+page_size]
			if end_addr != end_aligned and (addr % page_size + len(data)) > page_size:
				yield end_aligned, data[end_aligned-addr:]
		self._validate_addr(addr, 'write', aligned=False)
		self._validate_addr(addr + len(data), 'write end', aligned=False)
		for page_addr, chunk in paginate():
			self.log.info('Writing page 0x%x' % page_addr)
			self.write_page(page_addr, chunk)
			if verify:
				test_data = self.read(page_addr, len(chunk))
				if chunk != test_data:
					raise FlashError('write verification failed')

	@staticmethod
	def load(chip):
		import importlib.util
		import os.path
		mod = None

		if '.' in chip:
			# import external module
			try:
				name, _ = os.path.splitext(os.path.basename(chip))
				spec = importlib.util.spec_from_file_location(name, chip)
				mod = importlib.util.module_from_spec(spec)
				spec.loader.exec_module(mod)
			except FileNotFoundError:
				raise FlashPluginError('plugin %s not found' % chip)
			except IOError:
				raise FlashPluginError('can\'t load plugin %s' % chip)
		else:
			# import local module
			chip = chip.lower()
			try:
				mod = importlib.import_module('bdmdongle.flash_%s' % chip)
			except ModuleNotFoundError:
				raise FlashPluginError('there is no builtin support for %s' % chip)

		# find class inheriting from Flash
		flash_obj = None
		for name, obj in mod.__dict__.items():
			if isinstance(obj, type) and issubclass(obj, Flash) and obj is not Flash:
				flash_obj = obj
		if not flash_obj:
			raise FlashPluginError('plugin must define an object inheriting from Flash')

		return flash_obj
