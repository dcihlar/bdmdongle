import logging
import serial
import struct

__all__ = [ 'BDMDongle', 'Error', 'ProtocolError', 'CommandError' ]


class Error(Exception):
	pass

SerialError = serial.SerialException

class ProtocolError(Error):
	pass

class CommandError(Error):
	pass


class response:
	class OK():
		def read(self, ser, firstline):
			if firstline != 'OK':
				raise ProtocolError('unexpected response')
			return True

	class Line():
		def read(self, ser, firstline):
			return firstline

	class BinStart():
		def read(self, ser, firstline):
			if firstline != 'BIN':
				raise ProtocolError('unexpected response')

	class BinReader(BinStart):
		def read_one(self, ser):
			raise NotImplementedError('read_one not implemented')

		def read(self, ser, firstline):
			super(response.BinReader, self).read(ser, firstline)
			return self.read_one(ser)

	class Struct(BinReader):
		FMT = None

		def read_one(self, ser):
			data = ser.read(struct.calcsize('<' + self.FMT))
			return self.from_bin_avr(data)

		def from_bin_avr(self, data):
			return struct.unpack('<' + self.FMT, data)[0]

		def from_bin_cpu32(self, data):
			return struct.unpack('>' + self.FMT, data)[0]

		def to_bin_avr(self, val):
			return struct.pack('<' + self.FMT, val)

		def to_bin_cpu32(self, val):
			return struct.pack('>' + self.FMT, val)

	class Long(Struct):
		FMT = 'L'
	class Word(Struct):
		FMT = 'H'
	class Byte(Struct):
		FMT = 'B'

	@staticmethod
	def from_flag(name):
		if name == 'b': return response.Byte()
		if name == 'w': return response.Word()
		if name == 'l': return response.Long()
		raise ValueError('unknown type "%s"' % name)

	class Array(BinReader):
		def __init__(self, elemtype, numorvals):
			self.type = elemtype
			if type(numorvals) is int:
				self.num = numorvals
				self.vals = [0] * self.num
			else:
				self.vals = numorvals
				self.num = len(self.vals)

		def read_one(self, ser):
			self.vals = list(self.type.read_one(ser) for i in range(self.num))
			return self.vals

		def to_bin(self, arch='avr'):
			convmethod = self.type.to_bin_avr if arch == 'avr' else self.type.to_bin_cpu32
			return b''.join(convmethod(val) for val in self.vals)


class Sequence():
	def __init__(self, addr_or_seq, size=None, data=None):
		if isinstance(addr_or_seq, Sequence):
			self.address = addr_or_seq.address
			self.size = addr_or_seq.size
			self.data = addr_or_seq.data
		elif type(addr_or_seq) is tuple:
			self.address, self.size, self.data = addr_or_seq
		elif size is None or data is None:
			raise ValueError('invalid arguments')
		else:
			self.address = addr_or_seq
			self.size = size
			self.data = data


class BDMDongle():
	reg_names = tuple('D%d' % i for i in range(8)) + \
	            tuple('A%d' % i for i in range(8)) + \
	            ('RPC', 'PCC', 'SR', 'USP', 'SSP', 'SFC', 'DFC', 'ATEMP', 'FAR', 'VBR')
	reg_groups = {
		'a': reg_names,
		'g': reg_names[:16] + ('RPC', 'SR'),
	}

	def __init__(self):
		self._is_attached = None
		self.ser = None
		self.log = logging.getLogger('dongle')
		self.seqs = None

	@property
	def is_attached(self):
		# Note: False response is not necessarily valid
		#       (it may change to True at any point in time)
		if self._is_attached is None:
			if 'FROZEN' not in self.read_status():
				return False
			self._is_attached = True
		return True

	def poll_abort(self):
		return False

	def read_line(self, abortable=False):
		if abortable:
			self.ser.timeout = 0.1
			line = b''
			abort_requested = False
			while b'\n' not in line:
				try:
					line += self.ser.readline()
					if self.poll_abort() and not abort_requested:
						self.ser.write(b'\n')
						abort_requested = True
				except KeyboardInterrupt:
					self.ser.write(b'\n')
					abort_requested = True
		else:
			line = self.ser.readline()
		line = line.decode().strip()
		self.log.debug('<' + line)
		return line

	def connect(self, port):
		self.ser = serial.Serial(port, 115200, timeout=None)
		self.ser.reset_input_buffer()
		hello = self.read_line()
		if hello != 'BDMdongle':
			raise ProtocolError('didn\'t get hello')
		self.enter_binary()

	def read_response(self, resp):
		ret = self.read_line(abortable=True)
		if ret == 'ABORT':
			ret = 'OK'
		if ret.startswith('E:'):
			raise CommandError(ret[3:])
		return resp.read(self.ser, ret)

	def send_cmd(self, cmd, resp=response.OK()):
		self.log.debug('>' + cmd)
		self.ser.write((cmd + '\n').encode())
		return self.read_response(resp)

	def send_payload(self, cmd, vals, respsz=response.Long()):
		self.send_cmd(cmd, resp=response.BinStart())
		payload = response.Array(respsz, vals).to_bin()
		self.log.debug('»' + payload.hex())
		self.ser.write(payload)
		self.ser.write(b'\n')
		return self.read_response(response.OK())

	def enter_binary(self):
		while True:
			ret = self.send_cmd('b', resp=response.Line())
			if ret.upper() == 'BIN':
				return

	def attach(self):
		if not self.is_attached:
			self.send_cmd('a')
			self._is_attached = True
			self.log.info('attached')

	def read_status(self):
		return set(self.send_cmd('t', resp=response.Line()).split()[1:])

	def to_proto_reg_name(self, reg):
		reg = reg.upper()
		if reg not in self.reg_names:
			raise ValueError('invalid register')
		if reg == 'ATEMP':
			reg = 'TMP'
		return reg

	def read_reg(self, reg):
		self.attach()
		reg = self.to_proto_reg_name(reg)
		return self.send_cmd('p' + reg, resp=response.Long())

	def write_reg(self, reg, value):
		self.attach()
		reg = self.to_proto_reg_name(reg)
		self.send_cmd('P%s %x' % (reg, value))

	def read_multiple_regs(self, group='a'):
		self.attach()
		reg_names = self.reg_groups[group]
		vals = self.send_cmd('e' + group, resp=response.Array(response.Long(), len(reg_names)))
		return dict(zip(reg_names, vals))

	def write_multiple_regs(self, regs, group=None):
		towrnames = set(regs.keys())
		def count_diff(group):
			return len(set(self.reg_groups[group]) - towrnames)
		# find a group with minimum extra regs
		if group is None:
			group = min(self.reg_groups, key=count_diff)
		# prepare data set
		vals = []
		for reg_name in self.reg_groups[group]:
			vals.append(regs.get(reg_name, 0))
		# write them!
		self.attach()
		self.send_payload('E' + group, vals)

	def read_mem(self, address, size, count=1, packed=False, force_dump=False):
		self.attach()
		vals = []
		respsz = response.from_flag(size)
		if not force_dump:
			vals.append(self.send_cmd('@%c%x' % (size, address), resp=respsz))
			count -= 1
		if count > 0:
			vals += self.send_cmd('d%c%x' % (size, count), resp=response.Array(respsz, count))
		if not packed:
			return vals
		return response.Array(respsz, vals).to_bin(arch='cpu32')

	def read_mem_blob(self, address, size):
		nlongs = size // 4
		force_dump = True
		if nlongs:
			blob = self.read_mem(address, 'l', nlongs, packed=True)
			nbytes = size % 4
		elif size // 2:
			blob = self.read_mem(address, 'w', 1, packed=True)
			nbytes = size % 2
		else:
			blob = b''
			nbytes = size
			force_dump = False

		if nbytes:
			blob += self.read_mem(address + nlongs*4, 'b', nbytes, packed=True, force_dump=force_dump)
		return blob

	def write_mem(self, address, size, vals, force_fill=False):
		vals = list(vals)
		if not vals:
			return
		self.attach()
		respsz = response.from_flag(size)
		if not force_fill:
			firstval = vals.pop(0)
			self.send_cmd('=%c%x %x' % (size, address, firstval))
		if vals:
			self.send_payload('D%c%x' % (size, len(vals)), vals, respsz)

	def write_mem_blob(self, address, blob):
		def chunked(chunksz):
			chunksend = len(blob) & ~(chunksz-1)
			return [blob[pos:pos+chunksz] for pos in range(0, chunksend, chunksz)], blob[chunksend:]

		force_fill = True
		if len(blob) >= 4:
			chunks, leftover = chunked(4)
			vals = map(response.Long().from_bin_cpu32, chunks)
			self.write_mem(address, 'l', vals)
		elif len(blob) >= 2:
			chunks, leftover = chunked(2)
			vals = map(response.Word().from_bin_cpu32, chunks)
			self.write_mem(address, 'w', vals)
		else:
			leftover = blob
			force_fill = False

		if leftover:
			self.write_mem(address, 'b', leftover, force_fill=force_fill)

	def step_instruction(self):
		self.attach()
		self.send_cmd('s')

	def run(self, until=None, wait=False):
		if until is None:
			if self.is_attached:
				self.send_cmd('G' if wait else 'g')
				self._is_attached = None
		else:
			self.attach()
			self.send_cmd('u%x' % until)

	def reset(self, attach=True):
		if attach:
			self.send_cmd('R')
			self._is_attached = True
		else:
			self.send_cmd('r')
			self._is_attached = None

	def configure_sequencer(self, seqs):
		seqs = list(map(Sequence, seqs))
		for sid, seq in enumerate(seqs):
			self.send_cmd('q%x %x %c %x' % (sid, seq.address, seq.size, seq.data))
		self.send_cmd('q%x 0 b fff' % len(seqs))
		self.seqs = seqs

	def exec_sequencer(self, sid, addr=None, data=None, incaddr=True):
		if self.seqs is None:
			raise CommandError('sequencer not initialized')
		if sid is None or sid >= len(self.seqs):
			raise CommandError('invalid sequencer id')

		if addr is None and data is None:
			self.exec_sequencer_const()
			return

		if addr is None:
			addr = self.seqs[sid].address
		if data is None:
			data = [self.seqs[sid].data]
		respsz = response.from_flag(self.seqs[sid].size)
		self.send_payload('Q%x %x%c %x' % (sid, addr, '+' if incaddr else '=', len(data)), data, respsz)

	def exec_sequencer_const(self, count=1):
		if self.seqs is None:
			raise CommandError('sequencer not initialized')
		self.send_cmd('Q%x 0= %x' % (len(self.seqs), count))

	def sequencer_flash(self, sid, addr, data):
		if sid not in range(0, len(self.seqs)) or addr is None or data is None:
			raise ValueError('invalid argument')
		respsz = response.from_flag(self.seqs[sid].size)
		# this is going through a buffer so it needs chunking
		chunksz = 128
		for ofs in range(0, len(data), chunksz):
			chunk_size = min(chunksz, len(data) - ofs)
			self.send_payload('F%x %x %x' % (sid, addr + ofs, chunk_size), data[ofs:ofs+chunk_size], respsz)

	def flash_wait(self, addr):
		self.send_cmd('f%x' % addr)
